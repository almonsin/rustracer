/* rustracer - a raytracer in Rust
 * Copyright (c) 2016 Peter Belanyi (peter.belanyi@gmail.com)
 * 
 * rustracer is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * rustracer is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with rustracer. If not, see <http://www.gnu.org/licenses/>.
 */

use basetypes::*;
use ray::Ray;

pub struct Camera {
    pub pos      : Vector,
    pub right    : Vector,
    pub up       : Vector,
    pub top_left : Vector,
    pub res      : (usize,usize)
}

impl Camera {    
    pub fn new(pos: Vector, dir: Vector, up: Vector, fov: f32, ratio: f32, resolution: (usize,usize)) -> Camera {
        let dir = dir.normalize();
        let up = up.normalize();
        let right_unit = dir.cross(up);
        
        let screen_width = (fov.to_radians()/2.0).tan() * 2.0;
        let screen_height = screen_width / ratio;
        
        let right_full = right_unit * screen_width;
        let up_full = right_unit.cross(dir) * screen_height;
        
        let top_left = dir - right_full/2.0 + up_full/2.0;
        
        Camera {
            pos      : pos,
            right    : right_full,
            up       : up_full,
            top_left : top_left,
            res      : resolution,
        }
    }
    
    pub fn ray_for_pixel(&self, x: usize, y: usize) -> Ray {
        let dir_x = self.right * x as f32 / self.res.0 as f32;
        let dir_y = - self.up * y as f32 / self.res.1 as f32;
        
        Ray::new(self.pos, self.top_left + dir_x + dir_y)
    }
}
