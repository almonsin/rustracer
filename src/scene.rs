/* rustracer - a raytracer in Rust
 * Copyright (c) 2016 Peter Belanyi (peter.belanyi@gmail.com)
 * 
 * rustracer is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * rustracer is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with rustracer. If not, see <http://www.gnu.org/licenses/>.
 */

extern crate simple_json;
use self::simple_json::{ Json, JsonOption };

use std::fs::File;
use std::io::Read;
use std::collections::HashMap;

use basetypes::*;
use camera::Camera;
use shapes::*;


pub struct Material {
    pub color      : Color,
    pub diffuse    : f32,
    pub specular   : f32,
    pub spec_exp   : f32,
    pub reflection : f32,
    pub refraction : f32,
    pub refr_idx   : f32,
    pub absorbance : f32,
}

pub struct Light {
    pub pos   : Vector,
    pub color : Color,
}

pub struct Params {
    pub exposure            : f32,
    pub background_color    : Color,
    pub ambient_coefficient : f32,
    pub epsilon             : f32,
    pub max_depth           : u32,
}

pub struct Scene {
    pub materials: Vec<Material>,
    pub objects: Vec<(BoxedShape,usize)>,
    pub lights: Vec<Light>,
    pub camera: Camera,
    pub params: Params,
}

impl Scene {
    pub fn load(filename: &String) -> Result<Scene,String> {
        // load file contents into string
        let mut file = match File::open(filename) {
            Ok(file) => file,
            Err(err) => return Err(err.to_string()),
        };
        let mut contents = String::new();
        if let Err(err) = file.read_to_string(&mut contents) {
            return Err(err.to_string());
        }

        // parse json object from string
        let result = Json::parse(&contents);

        // load scene from json object
        let scene_obj = result.object();

        // helpers
        let get_vector = |obj: &HashMap<String,JsonOption>, name: &str, default| {
            match obj.get(name) {
                Some(val) => {
                    let a = val.array();
                    vec3(*a[0].number(), *a[1].number(), *a[2].number())
                },
                None => default,
            }
        };

        let get_float = |obj: &HashMap<String,JsonOption>, name: &str, default| {
            match obj.get(name) {
                Some(val) => *val.number(),
                None => default,
            }
        };

        let get_integer = |obj: &HashMap<String,JsonOption>, name: &str, default| {
            match obj.get(name) {
                Some(val) => *val.integer(),
                None => default,
            }
        };

        let get_string = |obj: &HashMap<String,JsonOption>, name: &str, default: &str| {
            match obj.get(name) {
                Some(val) => val.string().clone(),
                None => default.to_string(),
            }
        };

        let get_pair = |obj: &HashMap<String,JsonOption>, name: &str, default| {
            match obj.get(name) {
                Some(val) => {
                    let a = val.array();
                    (*a[0].integer() as usize, *a[1].integer() as usize)
                },
                None => default,
            }
        };

        // load materials
        let mut materials: Vec<Material> = Vec::new();
        let mut material_indices: HashMap<String,usize> = HashMap::new();

        for (name, material_option) in scene_obj["materials"].object() {
            let material_obj = material_option.object();
            let color = get_vector(material_obj, "color", vec3(1.0, 1.0, 1.0));
            let diffuse = get_float(material_obj, "diffuse", 1.0);
            let specular = get_float(material_obj, "specular", 1.0);
            let spec_exp = get_float(material_obj, "specular exponent", 50.0);
            let reflection = get_float(material_obj, "reflection", 0.0);
            let refraction = get_float(material_obj, "refraction", 0.0);
            let refr_index = get_float(material_obj, "refraction idx", 1.0);
            let absorbance = get_float(material_obj, "absorbance", 0.0);

            let index = materials.len();
            materials.push(Material{
                color: color,
                diffuse: diffuse,
                specular: specular,
                spec_exp: spec_exp,
                reflection: reflection,
                refraction: refraction,
                refr_idx: refr_index,
                absorbance: absorbance,
            });
            material_indices.insert(name.clone(), index);
        }

        // load objects
        let mut objects: Vec<(BoxedShape,usize)> = Vec::new();

        for object_option in scene_obj["objects"].array() {
            let object_obj = object_option.object();

            let material_name = get_string(object_obj, "material", "default");
            let material_index = material_indices[&material_name];

            let object_type = get_string(object_obj, "type", "unknown");
            let obj: BoxedShape = match &object_type as &str {
                "sphere" => {
                    let centre = get_vector(object_obj, "centre", vec3(1.0, 1.0, 1.0));
                    let radius = get_float(object_obj, "radius", 1.0);
                    Box::new(Sphere::new(centre, radius))
                },
                "plane" => {
                    let point = get_vector(object_obj, "point", vec3(1.0, 1.0, 1.0));
                    let normal = get_vector(object_obj, "normal", vec3(1.0, 1.0, 1.0));
                    Box::new(Plane::new(point, normal))
                },
                _ => return Err("unknown object type".to_string())
            };
            objects.push( (obj,material_index) );
        }

        // load lights
        let mut lights: Vec<Light> = Vec::new();

        for light_option in scene_obj["lights"].array() {
            let light_obj = light_option.object();

            let position = get_vector(light_obj, "position", vec3(1.0, 1.0, 1.0));
            let color = get_vector(light_obj, "color", vec3(1.0, 1.0, 1.0));
            lights.push(Light{pos: position, color: color});
        }

        // load camera
        let camera = {
            let camera_obj = scene_obj["camera"].object();
            let position = get_vector(camera_obj, "position", vec3(1.0, 1.0, 1.0));
            let dir = get_vector(camera_obj, "direction", vec3(1.0, 1.0, 1.0));
            let up = get_vector(camera_obj, "up", vec3(1.0, 1.0, 1.0));
            let fov = get_float(camera_obj, "field of view", 90.0);
            let resolution = get_pair(camera_obj, "resolution", (800,600));
            let ratio = get_float(camera_obj, "aspect ratio",
                    resolution.0 as f32/ resolution.1 as f32);
            Camera::new(position, dir, up, fov, ratio, resolution)
        };

        // load parameters
        let params = {
            let params_obj = scene_obj["parameters"].object();
            let exposure = get_float(params_obj, "exposure", 1.0);
            let background_color = get_vector(params_obj, "background color", vec3(0.0, 0.0, 0.0));
            let ambient_coefficient = get_float(params_obj, "ambient coefficient", 0.0);
            let epsilon = get_float(params_obj, "epsilon", 0.001);
            let max_depth = get_integer(params_obj, "maximum depth", 3) as u32;
            Params {
                exposure: exposure,
                background_color: background_color,
                ambient_coefficient: ambient_coefficient,
                epsilon: epsilon,
                max_depth: max_depth,
            }
        };

        Ok(Scene{
            materials: materials,
            objects: objects,
            lights: lights,
            camera: camera,
            params: params,
        })
    }
}
