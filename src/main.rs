/* rustracer - a raytracer in Rust
 * Copyright (c) 2016 Peter Belanyi (peter.belanyi@gmail.com)
 * 
 * rustracer is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * rustracer is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with rustracer. If not, see <http://www.gnu.org/licenses/>.
 */

extern crate imagefmt;
extern crate getopts;

use std::env;
use std::f32;
use std::thread;
use std::sync::Arc;
use std::sync::mpsc;
use std::collections::HashMap;

use getopts::Options;
use imagefmt::{ColFmt, ColType};

mod basetypes;
mod ray;
mod camera;
mod shapes;
mod scene;

use basetypes::*;
use ray::Ray;
use shapes::Shape;
use scene::Scene;


fn trace(ray: &Ray, scene: &Scene, depth: u32) -> Color {
    // find the closest item the ray hits
    let (hit_item,distance) = scene.objects.iter().fold( (None,0.0), |hit_info, item| {
        let (hit_item,distance) = hit_info;
        let &(ref shape, _) = item;
        match shape.intersect(&ray) {
            Some(dist) =>                           // the ray hit the shape
                match hit_item {
                    Some(_) =>                          // we already hit something previously
                        if dist<distance {
                            (Some(item), dist)
                        } else {
                            hit_info
                        },
                    None =>                             // this is the first thing we hit
                        (Some(item), dist),
                },
            None =>                                 // the ray didn't hit
                hit_info,
        }
    });

    // calculate shading
    let color = match hit_item {
        Some(item) => {
            let &(ref shape, material_index) = item;
            let material = &scene.materials[material_index];

            let hit_point = ray.get_point_at(distance);
            let normal = shape.normal_at(hit_point);

            // sum up all lights
            let lighting_color = scene.lights.iter().fold( Vector::zero(), |color_sum, light| {
                let light_dir = (light.pos - hit_point).normalize();

                let shadow_ray = Ray::new(hit_point+normal*scene.params.epsilon, light_dir);
                let mut in_shadow = false;

                for &(ref shape, _) in &scene.objects {
                    if let Some(_) = shape.intersect(&shadow_ray) {
                        in_shadow = true;
                        break;
                    }
                }

                if in_shadow {
                    color_sum
                } else {
                    let diffuse = if material.diffuse > 0.0 {
                        let intensity = normal.dot(light_dir).max(0.0) * material.diffuse;
                        material.color.mul_element_wise(light.color) * intensity
                    } else {
                        Color::zero()
                    };

                    let specular = if material.specular > 0.0 {
                        let view_dir = ray.dir;
                        let refl_dir = light_dir - 2.0 * light_dir.dot(normal) * normal;
                        let intensity = view_dir.dot(refl_dir).max(0.0).powf(material.spec_exp) * material.specular;
                        light.color * intensity
                    } else {
                        Color::zero()
                    };

                    color_sum + diffuse + specular
                }
            });

            // calculate reflection
            let reflection_color = if material.reflection > 0.0 && depth < scene.params.max_depth {
                let refl_dir = ray.dir - 2.0 * ray.dir.dot(normal) * normal;
                let refl_ray = Ray::new(hit_point+normal*scene.params.epsilon, refl_dir);

                let refl_color = trace(&refl_ray, &scene, depth + 1);
                material.color.mul_element_wise(refl_color) * material.reflection
            } else {
                Color::zero()
            };

            // calculate refraction
            let coses = |n: f32, normal: Vector, raydir: Vector| {
                let cos_i = -normal.dot(raydir);
                let cos_t2 = 1.0 - n * n * (1.0 - cos_i * cos_i);
                (cos_i, cos_t2)
            };

            let refraction_color = if material.refraction > 0.0 && depth < scene.params.max_depth {

                let n = 1.0 / material.refr_idx;
                let (cos_i, cos_t2) = coses(n, normal, ray.dir);

                if cos_t2 > 0.0 {
                    let refr_dir = (n * ray.dir) + (n * cos_i - cos_t2.sqrt()) * normal;
                    let refr_ray = Ray::new(hit_point+refr_dir*scene.params.epsilon, refr_dir);

                    // check if refracted ray intersects the same shape
                    if let Some(dist) = shape.intersect(&refr_ray) {
                        let hit_point2 = refr_ray.get_point_at(dist);
                        let normal2 = -shape.normal_at(hit_point2); // hit from inside, normal reversed
                        let (cos_i, cos_t2) = coses(n, normal2, refr_ray.dir);

                        let refr_dir2 = (n * refr_ray.dir) + (n * cos_i - cos_t2.sqrt()) * normal;
                        let refr_ray2 = Ray::new(hit_point2+refr_dir2*scene.params.epsilon, refr_dir2);

                        let absorbance = material.color * material.absorbance * -dist;
                        let transparency = Color::new(absorbance.x.exp(), absorbance.y.exp(), absorbance.z.exp());

                        let refr_color = trace(&refr_ray2, &scene, depth + 1);
                        transparency.mul_element_wise(refr_color) * material.refraction
                    } else {
                        let refr_color = trace(&refr_ray, &scene, depth + 1);
                        material.color.mul_element_wise(refr_color) * material.refraction
                    }

                } else {
                    Color::zero()
                }
            } else {
                Color::zero()
            };

            // add ambient lighting
            let ambient_color = material.color * scene.params.ambient_coefficient;

            lighting_color + reflection_color + refraction_color + ambient_color
        },
        None => {
            scene.params.background_color
        },
    };

    color
}

fn tonemap(color: Color, exposure: f32) -> (u8,u8,u8) {
    // taken from http://filmicgames.com/archives/75
    let color = color * exposure;
    
    let f = |x| {
        let x = 0f32 .max(x-0.004);
        ((x*(6.2*x+0.5))/(x*(6.2*x+1.7)+0.06) * 255.0) as u8
    };

    ( f(color.x), f(color.y), f(color.z) )
}

fn print_usage(program: &str, opts: Options) {
    println!("rustracer  Copyright (c) 2016 Peter Belanyi (peter.belanyi@gmail.com)");
    println!("This program comes with ABSOLUTELY NO WARRANTY; see LICENSE for details.");
    println!("This is free software, and you are welcome to redistribute it under certain conditions; see LICENSE for details.");
    println!("");
    println!("{}", opts.usage(&format!("Usage: {} [-h] [-t N] [-o FILENAME.{{png,bmp,tga}}] -s FILENAME.json", program)));
}

fn main() {
    // parse arguments
    let args: Vec<String> = env::args().collect();
    let program = &args[0];

    let mut opts = Options::new();
    opts.optflag("h", "help", "Show this usage message.");
    opts.optopt("s", "scene", "Name of the scene file to load.", "FILENAME.json");
    opts.optopt("o", "out", "Name of image file to save; default: out.png.", "FILENAME.{png,bmp,tga}");
    opts.optopt("t", "threads", "Number of threads to use for rendering; default: 1.", "N");

    let matches = match opts.parse(&args[1..]) {
        Ok(m)  => { m }
        Err(e) => { panic!(e.to_string()) }
    };

    if matches.opt_present("h") {
        print_usage(&program, opts);
        return;
    }

    let scene_filename = match matches.opt_str("s") {
        Some(filename) => filename,
        None => {
            println!("Error: scene file not specified.");
            print_usage(&program, opts);
            return;
        },
    };

    let image_filename = match matches.opt_str("o") {
        Some(filename) => filename,
        None => "out.png".to_string(),
    };

    let num_of_threads: usize = match matches.opt_str("t") {
        Some(num) => match num.parse() {
            Ok(num) => num,
            Err(e) => { panic!(e.to_string()) },
        },
        None => 1
    };

    // load scene
    println!("Loading scene from {}", scene_filename);
    let scene = Scene::load(&scene_filename).unwrap();
    let scene_arc = Arc::new(scene);

    // channels to communicate with the threads
    let (tx, rx) = mpsc::channel();

    // distribute the rows among threads optimally
    let (resx, resy) = scene_arc.camera.res;
    let num_rows_base = resy / num_of_threads;
    let num_extra_rows = resy % num_of_threads;
    let mut rows_sum = 0;

    println!("Starting rendering on {} threads...", num_of_threads);

    // start num_of_threads threads
    for t in 0..num_of_threads {
        // tells the thread which rows to render
        let first_row = rows_sum;
        let num_rows = num_rows_base + if t < num_extra_rows { 1 } else { 0 };
        println!("Thread {} rendering rows {} - {}", t, first_row, first_row+num_rows-1);

        let tx = tx.clone();
        let thread_scene = scene_arc.clone();

        thread::spawn(move || {
            let mut thread_buf: Vec<u8> = Vec::new();

            for y in first_row .. (first_row+num_rows) {
                for x in 0..resx {
                    let ray = thread_scene.camera.ray_for_pixel(x,y);

                    let color = trace(&ray, &thread_scene, 0);

                    // tone map and write pixel
                    let c = tonemap(color, thread_scene.params.exposure);
                    thread_buf.push(c.0);
                    thread_buf.push(c.1);
                    thread_buf.push(c.2);
                }
            }

            // send the thread index and buffer back to the main thread
            tx.send((t,thread_buf)).unwrap();
        });

        rows_sum += num_rows;
    }

    // receive result from threads and assemble buffer
    let mut buf: Vec<u8> = Vec::new();
    let mut result_map: HashMap<usize,Vec<u8>> = HashMap::new();
    for _ in 0..num_of_threads {
        let (t,thread_buf) = rx.recv().unwrap();
        result_map.insert(t,thread_buf);
    }

    println!("done.");

    // append the buffers in proper order
    for t in 0..num_of_threads {
        buf.extend_from_slice(&result_map[&t]);
    }

    // save to file
    println!("Saving image as {}", image_filename);
    imagefmt::write(image_filename, resx, resy, ColFmt::RGB, &buf, ColType::Color).unwrap();
}
