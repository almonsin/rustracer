/* rustracer - a raytracer in Rust
 * Copyright (c) 2016 Peter Belanyi (peter.belanyi@gmail.com)
 * 
 * rustracer is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * rustracer is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with rustracer. If not, see <http://www.gnu.org/licenses/>.
 */

use basetypes::*;
use ray::Ray;

pub trait Shape {
    fn intersect(&self, ray: &Ray) -> Option<f32>;
    fn normal_at(&self, point: Vector) -> Vector;
}

// Box is needed so that it can contain implementors (Sphere, Plane)
// Send+Sync is needed for sharing it between threads
pub type BoxedShape = Box<Shape+Send+Sync>;

pub struct Sphere {
    pub centre : Vector,
    pub radius : f32,
}

impl Sphere {
    pub fn new(centre: Vector, radius: f32) -> Sphere {
        Sphere {
            centre : centre,
            radius : radius,
        }
    }
}

impl Shape for Sphere {
    fn intersect(&self, ray: &Ray) -> Option<f32> {
        let omc = ray.orig - self.centre;
        
        let b = ray.dir.dot(omc);
        let c = omc.dot(omc) - self.radius * self.radius;
        
        let b2_c = b*b - c;
        
        if b2_c < 0.0 {
            None
        } else {
            let x1 = -b + b2_c.sqrt();
            let x2 = -b - b2_c.sqrt();
            
            if x1<0.0 && x2<0.0 {           // both points are behind
                None
            } else if x1>0.0 && x2>0.0 {    // both points are forward
                Some(x1.min(x2))
            } else {                        // one foward, one behind
                Some(x1.max(x2))                
            }
        }
    }    
    fn normal_at(&self, point: Vector) -> Vector {
        (point - self.centre).normalize()
    }
}

pub struct Plane {
    pub point  : Vector,
    pub normal : Vector,
}

impl Plane {
    pub fn new(point: Vector, normal: Vector) -> Plane {
        Plane {
            point  : point,
            normal : normal.normalize(),
        }
    }
}

impl Shape for Plane {
    fn intersect(&self, ray: &Ray) -> Option<f32> {
        let ddn = ray.dir.dot(self.normal);
        if ddn == 0.0 {
            None
        } else {
            let distance = self.normal.dot(self.point - ray.orig) / ddn;
            if distance < 0.0 { None } else { Some(distance) }
        }
    }
    fn normal_at(&self, _: Vector) -> Vector {
        self.normal
    }
}
