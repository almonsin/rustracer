rustracer
---------

rustracer is an open source raytracer implemented in the Rust programming language.
It was an excercise in learning Rust, so it's not particularly optimized for speed or feature completeness.

See rustracer website https://gitlab.com/almonsin/rustracer on GitLab.

Keywords: rust, raytracer

## Features implemented:

- multithreaded rendering
- primitives: sphere, plane
- rendering reflection and refraction
- HDR rendering and tone mapping
- loading scene description from JSON files
- saving rendered image into png, bmp and tga formats

## Features that could be added:

- antialiasing by supersampling
- soft shadows
- texture mapping
- more primitives
- acceleration structures for speeding up intersection testing

## Supported platforms:

Developements and tests are done under the following compilers / OSs:
- Rust 1.9 on Debian 8.4 Jessie

## Dependencies

See Cargo.toml

## Installation

After downloading the sources and changing into its directory:
- cargo build
- cargo run -- --help

## License

Copyright (c) 2016 Peter Belanyi (peter.belanyi@gmail.com)

Distributed under the terms of the GPLv3. See the LICENSE file that accompanies this distribution for the full text of the license.

## How to contribute

The most efficient way to help and contribute is to use the tools provided by GitLab:
- please fill bug reports and feature requests here: https://gitlab.com/almonsin/rustracer/issues
- fork the repository, make some small changes and submit them with pull-request

You can also email me directly, I will answer any questions and requests.